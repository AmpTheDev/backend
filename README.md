# AmpAPI
API managing authentication and other stuff for everything I make, hosted over at https://api.amplifiable.tk.
## Usage
### Building
To build the API from source, clone the `release` branch for stable versions or the `develop` branch for work in progress versions.
After cloning, run `./gradlew distTar` on Linux and macOS or `gradlew distZip` on windows.
The result will be in `build/distributions`.
### Running
To run the API, extract the distribution archive and run the `backend` script in the `bin` directory.
The API requires an active PostgreSQL server.

The default settings will be fine for most people, but if you want to change some edit
`ampcraft.config.json`. Most default values are randomly generated UUIDs.

The port the API will listen on is 9090 by default. To change it, edit `resources/application.conf` and rebuild the API.
## Contributing
All contributions are welcome. If you want to fork the API and use it as your own,
that's fine too, just link this repository somewhere. To fix the `deploy` CI stage failing,
go to GitLab's CI settings and expand the variables section.
Set `SSH_USER` to the user the script should log in as, `SSH_IP` to the IP it should
ssh in to, `SSH_KNOWN_HOSTS` to the output of `ssh-keyscan $SSH_IP` and `SSH_PRIVATE_KEY` to a private key which it can connect to the server with.
On the server, create a `web` directory in the home folder of `$SSH_USER` and place a file called `extract.sh` in it with the following content:
```shell script
#!/bin/bash
echo "[VPS] Changing directory to web data"
cd $HOME/web
echo "[VPS] Deleting old backend files"
rm -rf backend-backend
echo "[VPS] Extracting new backend files"
tar -xf backend.tar
echo "[VPS] Restarting API service"
echo "<root password>" | su -c "systemctl restart ampapi" root 2> /dev/null
```
Replace `<root password>` with the root password you've set, or remove the line and
manually restart the API. This script assumes you've set up a SystemD service called `ampapi`
which manages the API, for example:
```
[Unit]
Description=AmpAPI
Wants=network-online.target
After=network-online.target

[Service]
User=<user>
Group=<user>
ExecStart=/home/<user>/web/backend-backend/bin/backend
WorkingDirectory=/home/<user>/web/backend-backend/bin
Restart=always
RestartSec=10

[Install]
WantedBy=default.target
```
Replace `<user>` with the username you chose to use.
Running the API as root is not recommended, as a vulnerability that normally isn't
super harmful can destroy your system.