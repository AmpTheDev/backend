@file:Suppress("EXPERIMENTAL_API_USAGE")

package tk.amplifiable.backend

import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.application.log
import io.ktor.auth.Authentication
import io.ktor.auth.authenticate
import io.ktor.auth.basic
import io.ktor.auth.jwt.jwt
import io.ktor.features.*
import io.ktor.gson.gson
import io.ktor.http.HttpStatusCode
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.Locations
import io.ktor.response.respond
import io.ktor.routing.routing
import org.isomorphism.util.TokenBucket
import org.isomorphism.util.TokenBuckets
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.transaction
import tk.amplifiable.backend.authentication.JwtConfig
import tk.amplifiable.backend.authentication.User
import tk.amplifiable.backend.authentication.Users
import tk.amplifiable.backend.authentication.hashPassword
import tk.amplifiable.backend.config.Configuration
import tk.amplifiable.backend.endpoints.EndpointRegistry
import tk.amplifiable.backend.extensions.ExtensionManager
import tk.amplifiable.backend.services.ServiceRegistry
import tk.amplifiable.backend.utils.*
import java.io.File
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.HashMap
import kotlin.collections.set

val mainConfig = Configuration(File("ampapi.config.json"))
var setupUser: User? = null

fun getSetupUserE() = setupUser!!

fun main(args: Array<String>) {
    io.ktor.server.netty.EngineMain.main(args)
}

@KtorExperimentalLocationsAPI
@Suppress("unused") // Referenced in application.conf
fun Application.module() {
    install(DefaultHeaders)
    install(CallLogging)
    install(ConditionalHeaders)
    install(Locations) {
    }
    install(ForwardedHeaderSupport)
    install(XForwardedHeaderSupport)

    val extensionManager = ExtensionManager()
    extensionManager.addJarsToClasspath()
    extensionManager.searchInClasspath()
    extensionManager.load()

    Database.connect(
        "jdbc:${mainConfig.data.databaseType.jdbcType}://${mainConfig.data.sqlHost}:${mainConfig.data.sqlPort}/${mainConfig.data.sqlDatabase}",
        driver = mainConfig.data.databaseType.driver,
        user = mainConfig.data.sqlUser,
        password = mainConfig.data.sqlPassword
    )

    setupUser = transaction {
        SchemaUtils.createMissingTablesAndColumns(Users)
        User.findById(mainConfig.data.setupUuid) ?: User.new(mainConfig.data.setupUuid) {
            username = UUID.randomUUID().toString()
            email = UUID.randomUUID().toString()
            services = arrayOf("admin")
            lastPwReset = 0
            statusId = UUID.randomUUID()
            password =
                "the hash function can't return this password as it's not hex and isn't the right amount of characters"
        }
    }

    extensionManager.enable(this)
    ServiceRegistry.registerEndpoints()

    log.info("Setup Token: ${JwtConfig.makeToken("admin", getSetupUserE())}")

    val rateLimiters = HashMap<String, TokenBucket>()
    install(Authentication) {
        basic(name = "password") {
            realm = mainConfig.data.jwtRealm
            validate { credentials ->
                if (!mainConfig.data.testServer) {
                    if (!rateLimiters.containsKey(request.origin.remoteHost)) {
                        rateLimiters[request.origin.remoteHost] = TokenBuckets.builder()
                            .withCapacity(6)
                            .withFixedIntervalRefillStrategy(1, 1, TimeUnit.HOURS)
                            .build()
                    }
                    if (!rateLimiters[request.origin.remoteHost]!!.tryConsume()) {
                        return@validate null
                    }
                }
                transaction {
                    User.find { Users.email eq credentials.name }
                        .find { it.password == hashPassword(credentials.password) }
                }
            }
        }
        for (service in ServiceRegistry.services.values) {
            jwt(name = "${service.getName()}-token") {
                verifier(JwtConfig.verifier)
                realm = mainConfig.data.jwtRealm
                validate {
                    tokenAuth(it, service.getName(), service.setupUserAllowed())
                }
            }
        }
        jwt(name = "reset-token") {
            verifier(JwtConfig.verifier)
            realm = mainConfig.data.jwtRealm
            validate {
                // doesn't use tokenAuth() as it's a completely different authentication process
                val uuidClaim = it.payload.getClaim("uuid") ?: return@validate null
                val uuidStr = uuidClaim.asString() ?: return@validate null
                val uuid = UUID.fromString(uuidStr)

                transaction {
                    User.find { Users.id eq uuid }.find { a -> it.payload.getClaim("made_at").asLong() > a.lastPwReset }
                }
            }
        }
    }

    install(ContentNegotiation) {
        gson {
        }
    }

    routing {
        for (endpoint in EndpointRegistry.endpoints) {
            if (endpoint.authentication != null) {
                authenticate(endpoint.authentication) {
                    endpoint(endpoint.method, endpoint.url, endpoint.callback)
                }
            } else {
                endpoint(endpoint.method, endpoint.url, endpoint.callback)
            }
        }
    }

    install(StatusPages) {
        exception<ErrorException> {
            call.respondError(it.error)
        }

        exception<Exception> {
            log.error("Internal error: ", it)
            call.respondError(ErrorType.INTERNAL_ERROR)
        }

        for (code in HttpStatusCode.allStatusCodes) {
            if (code.value in 200..299) {
                continue
            }
            status(code) {
                call.respond(
                    it,
                    mapOf(
                        "error" to it.description,
                        "type" to ErrorType.HTTP_ERROR_CODE,
                        "success" to false,
                        "code" to code.value
                    )
                )
            }
        }
    }
}