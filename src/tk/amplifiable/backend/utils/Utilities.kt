package tk.amplifiable.backend.utils

import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.auth.jwt.JWTCredential
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.response.respond
import io.ktor.routing.*
import org.apache.commons.mail.DefaultAuthenticator
import org.apache.commons.mail.HtmlEmail
import org.apache.commons.mail.SimpleEmail
import org.jetbrains.exposed.sql.transactions.transaction
import tk.amplifiable.backend.authentication.User
import tk.amplifiable.backend.authentication.Users
import tk.amplifiable.backend.getSetupUserE
import tk.amplifiable.backend.mainConfig
import java.util.*
import kotlin.collections.HashMap

fun ApplicationCall.tokenAuth(
    input: JWTCredential,
    thisService: String = "default",
    setupUserAcceptable: Boolean = false
): User? {
    val uuidClaim = input.payload.getClaim("id") ?: return null
    val serviceClaim = input.payload.getClaim("service") ?: return null
    val statusClaim = input.payload.getClaim("id2") ?: return null
    if (serviceClaim.asString() != thisService) {
        return null
    }
    val uuid: UUID
    val status: UUID
    try {
        uuid = UUID.fromString(uuidClaim.asString())
        status = UUID.fromString(statusClaim.asString())
    } catch (ex: Exception) {
        return null
    }
    if (uuid == mainConfig.data.setupUuid) {
        return if (setupUserAcceptable) getSetupUserE() else null
    }
    return transaction {
        User.find { Users.id eq uuid }
            .find { it.statusId == status && it.services.contains(thisService) }
    }
}

fun Route.endpoint(method: HttpMethod, url: String, callback: suspend (call: ApplicationCall) -> Unit) {
    when (method) {
        HttpMethod.Get -> {
            get(url) {
                callback(call)
            }
        }
        HttpMethod.Post -> {
            post(url) {
                callback(call)
            }
        }
        HttpMethod.Put -> {
            put(url) {
                callback(call)
            }
        }
        HttpMethod.Patch -> {
            patch(url) {
                callback(call)
            }
        }
        HttpMethod.Delete -> {
            delete(url) {
                callback(call)
            }
        }
        HttpMethod.Head -> {
            head(url) {
                callback(call)
            }
        }
        HttpMethod.Options -> {
            options(url) {
                callback(call)
            }
        }
        else -> throw IllegalArgumentException()
    }
}

fun sendMail(address: String, subject: String, content: String, html: Boolean = true) {
    val email = if (html) HtmlEmail() else SimpleEmail()
    email.hostName = mainConfig.data.smtpHost
    email.setSmtpPort(mainConfig.data.smtpPort)
    email.setAuthenticator(DefaultAuthenticator(mainConfig.data.emailUsername, mainConfig.data.emailPassword))
    email.isSSLOnConnect = mainConfig.data.sslOnConnect
    email.setFrom(mainConfig.data.emailAddress)
    email.subject = subject
    email.setMsg(content)
    email.addTo(address)
    email.send()
}

suspend fun ApplicationCall.respondSuccess(data: Map<String, Any> = HashMap()) {
    respond(
        if (data.isEmpty()) HttpStatusCode.NoContent else HttpStatusCode.OK,
        if (data.isNotEmpty()) successMap(data) else HashMap()
    )
}

suspend fun ApplicationCall.respondError(error: ErrorTypeInterface) {
    respond(error.code(), errorMap(error))
}

suspend fun ApplicationCall.respondError(error: Error) {
    respond(error.type.code(), errorMap(error))
}

fun errorMap(error: ErrorTypeInterface): Map<String, Any> {
    return errorMap(error.default())
}

fun errorMap(error: Error): Map<String, Any> {
    return mapOf("error" to error.description, "type" to error.type.getType(), "success" to false).plus(error.extraData)
}

fun successMap(data: Map<String, Any> = HashMap()): Map<String, Any> {
    return mapOf("success" to true).plus(data)
}