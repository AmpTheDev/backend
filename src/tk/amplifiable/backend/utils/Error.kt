package tk.amplifiable.backend.utils

import io.ktor.http.HttpStatusCode

data class Error(
    val type: ErrorTypeInterface,
    val extraData: Map<String, Any> = HashMap(),
    val description: String = type.description()
)

/**
 * Either I'm stupid or it's not possible, but I can't figure out how to have overridable enum methods with bodies so I'm using an interface
 */
interface ErrorTypeInterface {
    fun description(): String

    fun default(): Error

    fun code() = HttpStatusCode.NotAcceptable

    fun getType(): String
}

enum class ErrorType : ErrorTypeInterface {
    INVALID_PARAMETERS {
        override fun description() = "Invalid parameters"

        override fun code() = HttpStatusCode.UnprocessableEntity
    },
    TOKEN_INVALID {
        override fun description() = "The token is invalid or has expired"
    },
    INTERNAL_ERROR {
        override fun description() = "Internal error"

        override fun code() = HttpStatusCode.InternalServerError
    },
    HTTP_ERROR_CODE {
        override fun description() = "HTTP error code"

        override fun default() = Error(this, mapOf("code" to 404), "Not Found")
    };

    override fun default() = Error(this)

    override fun getType() = name
}