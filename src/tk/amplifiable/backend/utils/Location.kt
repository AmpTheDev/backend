@file:Suppress("EXPERIMENTAL_API_USAGE")

package tk.amplifiable.backend.utils

import io.ktor.locations.Location

@Location("/errors")
class ErrorListLocation

@Location("/authenticate")
class AuthenticateLocation

@Location("/changepassword")
class ChangePasswordLocation

@Location("/forgotpassword/{email}")
class ForgotPasswordLocation(val email: String, val includeToken: Boolean = false)

@Location("/confirmpasswordchange/{token}")
class ConfirmPasswordChangeLocation(val token: String)

@Location("/admin/users/create")
class CreateUserLocation

@Location("/admin/users/changepassword")
class AdminChangePasswordLocation

@Location("/admin/users/revokeservice")
class RevokeServiceLocation

@Location("/admin/users/grantservice")
class GrantServiceLocation

