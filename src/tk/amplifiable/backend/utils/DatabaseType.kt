package tk.amplifiable.backend.utils

enum class DatabaseType(val jdbcType: String, val driver: String) {
    POSTGRESQL("postgresql", "org.postgresql.Driver"),
    MYSQL("mysql", "com.mysql.jdbc.Driver")
}