package tk.amplifiable.backend.utils

class ErrorException(val error: Error) : RuntimeException()

class UserExistsException : RuntimeException()

class UserDoesNotExistException : RuntimeException()