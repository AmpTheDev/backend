package tk.amplifiable.backend.extensions;

import io.ktor.application.Application;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tk.amplifiable.backend.services.Service;
import tk.amplifiable.backend.services.ServiceRegistry;

/**
 * Helper class to make creating Java extensions easier.
 */
public class JavaExtension implements Extension {
    @NotNull
    @Override
    public Logger getLogger() {
        return LoggerFactory.getLogger(getClass().getSimpleName());
    }

    @Override
    public void registerService(@NotNull Service service) {
        ServiceRegistry.Companion.registerService(service);
    }

    @Override
    public void load(@NotNull ExtensionManager manager) {

    }

    @Override
    public void enable(@NotNull ExtensionManager manager, @NotNull Application application) {

    }

    @Override
    public void unload(@NotNull ExtensionManager manager) {

    }

    @Override
    public void unenable(@NotNull ExtensionManager manager) {

    }
}
