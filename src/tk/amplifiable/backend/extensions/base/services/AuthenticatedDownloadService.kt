package tk.amplifiable.backend.extensions.base.services

import io.ktor.http.HttpMethod
import io.ktor.response.respondFile
import tk.amplifiable.backend.services.Service
import tk.amplifiable.backend.utils.ErrorType
import tk.amplifiable.backend.utils.respondError
import java.io.File

abstract class AuthenticatedDownloadService : Service {
    abstract fun getDownloadLocation(id: String): File

    override fun registerEndpoints() {
        registerEndpoint("/" + getName() + "/download/{id}", HttpMethod.Get, true) {
            val id = it.parameters["id"]
            if (id == null) {
                it.respondError(ErrorType.INVALID_PARAMETERS)
            }
            it.respondFile(getDownloadLocation(id!!))
        }
    }
}