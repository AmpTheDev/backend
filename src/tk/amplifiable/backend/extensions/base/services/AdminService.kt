package tk.amplifiable.backend.extensions.base.services

import io.ktor.http.HttpMethod
import io.ktor.request.receive
import org.jetbrains.exposed.sql.transactions.transaction
import tk.amplifiable.backend.authentication.*
import tk.amplifiable.backend.services.Service
import tk.amplifiable.backend.utils.ErrorException
import tk.amplifiable.backend.utils.ErrorType
import tk.amplifiable.backend.utils.respondSuccess
import java.util.*

@Suppress("EXPERIMENTAL_API_USAGE")
class AdminService : Service {
    override fun getName() = "admin"

    override fun setupUserAllowed() = true

    override fun registerEndpoints() {
        registerEndpoint("/admin/users/create", HttpMethod.Post, true) {
            val payload = it.receive<CreateUserPayload>()
            @Suppress("SENSELESS_COMPARISON")
            if (payload.email == null || payload.username == null || payload.password == null || payload.accessibleServices == null) {
                throw ErrorException(ErrorType.INVALID_PARAMETERS.default())
            }
            val userObj = transaction {
                User.new(UUID.randomUUID()) {
                    username = payload.username
                    password = hashPassword(payload.password)
                    email = payload.email
                    services = payload.accessibleServices + "default"
                    statusId = UUID.randomUUID()
                    lastPwReset = System.currentTimeMillis()
                }
            }
            it.respondSuccess(
                mapOf(
                    "uuid" to userObj.id.value,
                    "name" to userObj.username,
                    "email" to userObj.email
                )
            )
        }
        registerEndpoint("/admin/users/changepassword", HttpMethod.Post, true) {
            val payload = it.receive<ChangePasswordPayload>()
            transaction {
                val user = User.findById(payload.uuid)
                user?.password = hashPassword(payload.newPassword)
            }
            it.respondSuccess()
        }
        registerEndpoint("/admin/users/grantservice", HttpMethod.Post, true) {
            val payload = it.receive<GrantServicePayload>()
            transaction {
                val user = User.findById(payload.uuid)!!
                user.services += payload.service
            }
            it.respondSuccess()
        }
        registerEndpoint("/admin/users/revokeservice", HttpMethod.Post, true) { call ->
            val payload = call.receive<RevokeServicePayload>()
            transaction {
                val user = User.findById(payload.uuid)!!
                user.services = user.services.filter { it != payload.service }.toTypedArray()
            }
            call.respondSuccess()
        }
    }
}