package tk.amplifiable.backend.extensions.base.services

import io.ktor.auth.principal
import io.ktor.http.ContentType
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.response.respondText
import org.jetbrains.exposed.sql.transactions.transaction
import tk.amplifiable.backend.authentication.*
import tk.amplifiable.backend.endpoints.EndpointRegistry
import tk.amplifiable.backend.mainConfig
import tk.amplifiable.backend.services.Service
import tk.amplifiable.backend.utils.*
import java.io.BufferedReader
import java.util.*
import kotlin.collections.HashMap
import kotlin.collections.set

@Suppress("EXPERIMENTAL_API_USAGE")
class MainService : Service {
    private val pwResetSource =
        JwtConfig::class.java.getResourceAsStream("/passwordreset.html").bufferedReader(Charsets.UTF_8)
            .use(BufferedReader::readText)
    private val invalidSource =
        JwtConfig::class.java.getResourceAsStream("/invalidtoken.html").bufferedReader(Charsets.UTF_8)
            .use(BufferedReader::readText)

    override fun getName(): String = "default"

    override fun registerEndpoints() {
        EndpointRegistry.registerEndpoint("/authenticate", HttpMethod.Post, authentication = "password") {
            val payload = it.receive<AuthenticatePayload>()
            @Suppress("SENSELESS_COMPARISON")
            if (payload.service == null) {
                throw ErrorException(ErrorType.INVALID_PARAMETERS.default())
            }
            val user = it.principal<User>()!!
            if (!user.services.contains(payload.service)) {
                it.respond(HttpStatusCode.Unauthorized)
            } else {
                it.respondSuccess(
                    mapOf(
                        "uuid" to user.id.value,
                        "name" to user.username,
                        "token" to JwtConfig.makeToken(
                            payload.service,
                            user
                        ),
                        "service" to payload.service
                    )
                )
            }
        }
        registerEndpoint("/verify/{token}", HttpMethod.Get) { call ->
            try {
                val payload = JwtConfig.verifier.verify(call.parameters["token"]!!)
                val uuidClaim = payload.getClaim("id") ?: throw IllegalStateException()
                val serviceClaim = payload.getClaim("service") ?: throw IllegalStateException()
                val statusClaim = payload.getClaim("id2") ?: throw IllegalStateException()
                val uuid: UUID
                val status: UUID
                uuid = UUID.fromString(uuidClaim.asString())
                status = UUID.fromString(statusClaim.asString())
                if (uuid != mainConfig.data.setupUuid) {
                    transaction {
                        User.find { Users.id eq uuid }
                            .find { it.statusId == status }
                    } ?: throw IllegalStateException()
                    call.respondSuccess(mapOf("service" to serviceClaim.asString(), "uuid" to uuidClaim.asString()))
                }
            } catch (ignored: Exception) {

            }
            call.respondError(ErrorType.TOKEN_INVALID.default())
        }
        EndpointRegistry.registerEndpoint("/confirmpasswordchange", HttpMethod.Post, authentication = "reset-token") {
            val payload = it.receive<NewPasswordPayload>()
            val userPassed = it.principal<User>()!!
            transaction {
                val user = User.findById(userPassed.id.value)
                user?.password = hashPassword(payload.newPassword)
                user?.lastPwReset = System.currentTimeMillis()
                user?.statusId = UUID.randomUUID()
            }
            it.respondSuccess()
        }

        registerEndpoint("/confirmpasswordchange/{token}", HttpMethod.Get) {
            try {
                val token = it.parameters["token"]
                if (token != null) {
                    val payload = JwtConfig.verifier.verify(token)
                    val uuidClaim = payload.getClaim("uuid")
                    val uuidStr = uuidClaim?.asString()
                    if (uuidStr != null) {
                        val uuid = UUID.fromString(uuidStr)
                        val user = transaction {
                            User.findById(uuid)
                        }
                        if (user != null && user.lastPwReset < payload.getClaim("made_at").asLong()) {
                            it.respondText(
                                pwResetSource.replace(
                                    "\${AMPAPI_RESET_TOKEN}",
                                    token
                                ).replace("\${AMPAPI_USERNAME}", user.username), ContentType.Text.Html
                            )
                            return@registerEndpoint
                        }
                    }
                }
            } catch (ex: java.lang.Exception) {

            }
            it.respondText(invalidSource, ContentType.Text.Html)
        }

        registerEndpoint("/forgotpassword/{email}", HttpMethod.Get) {
            val email = it.parameters["email"] ?: throw ErrorException(ErrorType.INVALID_PARAMETERS.default())
            val includeTokenStr = it.request.queryParameters["includeToken"] ?: "false"
            val includeToken = includeTokenStr.toBoolean()
            val user = transaction {
                User.find { Users.email eq email }.firstOrNull()
            }
            if (user != null) {
                val token = JwtConfig.makePasswordResetToken(user.id.value)
                val content = if (!includeToken) "<html>" +
                        "<body>" +
                        "<p>Click <a href=\"${mainConfig.data.rootEndpoint}/confirmpasswordchange/$token\">here</a> to reset your password.<br />" +
                        "This link expires in 1 hour.</p>" +
                        "</body>" +
                        "</html>" else "<html>" +
                        "<body>" +
                        "<p>Your reset token is <b>$token</b>. (the last dot is <b>not</b> a part of the token.)<br />" +
                        "<br />" +
                        "You can also click <a href=\"${mainConfig.data.rootEndpoint}/confirmpasswordchange/$token\">here</a> to reset your password online.<br />" +
                        "The token and link expire in 1 hour.</p>" +
                        "</body>" +
                        "</html>"
                sendMail(email, "Password Reset", content)
            }
            it.respondSuccess()
        }

        registerEndpoint("/errors", HttpMethod.Get) {
            val map = HashMap<String, Any>()
            for (error in ErrorType.values()) {
                map[error.name] = errorMap(error.default())
            }
            it.respondSuccess(map)
        }

        registerEndpoint("/test", HttpMethod.Get) {
            it.respondText("<html><head><title>On Server!</title></head><body>YEET</body></html>", ContentType.Text.Html)
        }

        /*registerEndpoint("/endpoints", HttpMethod.Get) {
            val map = HashMap<String, Any>()
            for (endpoint in EndpointRegistry.endpoints) {
                val effectiveUrl = if (endpoint.url.startsWith("/")) endpoint.url.substring(0) else endpoint.url
                val parts = effectiveUrl.split('/')
                @Suppress("UNCHECKED_CAST") var currentMap = map.computeIfAbsent(endpoint.method.value) {
                    HashMap<String, Any>()
                } as HashMap<String, Any>
                for (directory in parts.subList(0, parts.size - 1)) {
                    if (directory.trim().isEmpty()) {
                        continue
                    }
                    if (!currentMap.containsKey(directory)) {
                        currentMap[directory] = HashMap<String, Any>()
                    }
                    @Suppress("UNCHECKED_CAST")
                    currentMap = currentMap[directory]!! as HashMap<String, Any>
                }
                currentMap[parts[parts.size - 1]] = SerializeEndpointData(endpoint.authentication)
            }
            it.respondSuccess(map)
        }*/
    }

    private data class SerializeEndpointData(val authentication: String?)
}