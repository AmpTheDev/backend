package tk.amplifiable.backend.extensions.base

import io.ktor.application.Application
import tk.amplifiable.backend.extensions.Extension
import tk.amplifiable.backend.extensions.ExtensionManager
import tk.amplifiable.backend.extensions.base.services.AdminService
import tk.amplifiable.backend.extensions.base.services.MainService

class BaseExtension : Extension {
    override fun enable(manager: ExtensionManager, application: Application) {
        registerService(AdminService())
        registerService(MainService())
    }
}