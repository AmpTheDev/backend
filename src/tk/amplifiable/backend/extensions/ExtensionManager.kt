package tk.amplifiable.backend.extensions

import com.google.gson.JsonParser
import io.ktor.application.Application
import org.slf4j.LoggerFactory
import java.io.File
import java.net.URL
import java.net.URLClassLoader

class ExtensionClassLoader(parent: ClassLoader) : URLClassLoader(arrayOf<URL>(), parent) {
    public override fun addURL(url: URL?) = super.addURL(url)
}

data class ExtensionData(
    val extension: Extension,
    val name: String,
    val id: String,
    val mainClass: String,
    val dependencies: List<String> = ArrayList(),
    val optionalDependencies: List<String> = ArrayList(),
    val loadsAfter: ArrayList<String> = ArrayList(),
    val loadBefore: ArrayList<String> = ArrayList(),
    val loadsBefore: ArrayList<String> = ArrayList()
)

class ExtensionManager {
    companion object {
        private val directory = File("extensions")
        private val logger = LoggerFactory.getLogger(ExtensionManager::class.java)
    }

    private val extensions = HashMap<String, ExtensionData>()
    private val loadedExtensions = ArrayList<String>()
    private val enabledExtensions = ArrayList<String>()
    private val loadOrder = ArrayList<String>()
    private val extensionClassLoader = ExtensionClassLoader(ExtensionManager::class.java.classLoader)

    fun addJarsToClasspath() {
        if (!directory.exists()) directory.mkdirs()
        val files = directory.listFiles()
        if (files != null) {
            for (file in files) {
                if (file.name.endsWith(".jar")) {
                    extensionClassLoader.addURL(file.toURI().toURL())
                }
            }
        }
    }

    fun searchInClasspath() {
        extensions.clear()
        loadOrder.clear()
        loadedExtensions.clear()
        logger.trace("Searching for extensions in classpath")
        for (extensionUrl in extensionClassLoader.getResources("META-INF/extensions.json")) {
            try {
                logger.trace("Attempting to load extension JSON at {}", extensionUrl)
                val jsonString = extensionUrl.openStream().bufferedReader(Charsets.UTF_8).readText()
                logger.trace("Parsing JSON {}", jsonString)
                val jsonObject = JsonParser().parse(jsonString).asJsonObject
                logger.trace("Got JsonObject")
                for (entry in jsonObject.entrySet()) {
                    try {
                        logger.trace("Attempting to load extension {}", entry.key)
                        val jobj = entry.value.asJsonObject
                        val name = jobj["name"].asString
                        val mainClass = jobj["main_class"].asString
                        val dependencies = ArrayList<String>()
                        if (jobj.has("dependencies")) {
                            for (element in jobj["dependencies"].asJsonArray) {
                                dependencies.add(element.asString)
                            }
                        }

                        val optionalDependencies = ArrayList<String>()
                        if (jobj.has("optional_dependencies")) {
                            for (element in jobj["optional_dependencies"].asJsonArray) {
                                optionalDependencies.add(element.asString)
                            }
                        }

                        val loadBefore = ArrayList<String>()
                        if (jobj.has("load_before")) {
                            for (element in jobj["load_before"].asJsonArray) {
                                loadBefore.add(element.asString)
                            }
                        }
                        var clazz: Class<*>
                        clazz = try {
                            Class.forName(mainClass)
                        } catch (ex: Exception) {
                            Class.forName(mainClass, true, extensionClassLoader)
                        }
                        if (!Extension::class.java.isAssignableFrom(clazz)) {
                            throw IllegalArgumentException("Extension class ${clazz.name} doesn't extend Extension")
                        }
                        val extensionObj = clazz.constructors.first().newInstance() as Extension
                        val data = ExtensionData(
                            extensionObj,
                            name,
                            entry.key,
                            mainClass,
                            dependencies,
                            optionalDependencies,
                            ArrayList(),
                            loadBefore
                        )
                        extensions[entry.key] = data
                        logger.trace("Got extension data $data")
                    } catch (ex: Exception) {
                        logger.error("Failed to load extension {} (defined at {})", entry.key, extensionUrl, ex)
                    }
                }
            } catch (ex: Exception) {
                logger.error("Failed to load extension defined at {}", extensionUrl, ex)
            }
        }
        logger.debug("Building dependency tree")
        logger.trace("Determining which extensions depend on eachother")
        for (entry in extensions) {
            for (dependency in entry.value.dependencies) {
                if (extensions.containsKey(dependency)) {
                    if (!entry.value.loadsAfter.contains(dependency)) {
                        entry.value.loadsAfter.add(dependency)
                    }
                    if (!extensions[dependency]!!.loadsBefore.contains(entry.key)) {
                        extensions[dependency]!!.loadsBefore.add(entry.key)
                    }
                } else {
                    throw IllegalStateException("Extension ${entry.value.name} depends on $dependency, but it is not available.")
                }
            }
            for (loadBefore in entry.value.loadBefore) {
                if (extensions.containsKey(loadBefore)) {
                    if (!extensions[loadBefore]!!.loadsAfter.contains(entry.key)) {
                        extensions[loadBefore]!!.loadsAfter.add(entry.key)
                    }
                    if (!entry.value.loadsBefore.contains(loadBefore)) {
                        entry.value.loadsBefore.add(loadBefore)
                    }
                } else {
                    throw IllegalStateException("Extension ${entry.value.name} is supposed to load before $loadBefore, but it is not available.")
                }
            }
            for (optionalDependency in entry.value.optionalDependencies) {
                if (extensions.containsKey(optionalDependency)) {
                    if (!entry.value.loadsAfter.contains(optionalDependency)) {
                        entry.value.loadsAfter.add(optionalDependency)
                    }
                    if (!extensions[optionalDependency]!!.loadsBefore.contains(entry.key)) {
                        extensions[optionalDependency]!!.loadsBefore.add(entry.key)
                    }
                }
            }
        }
        logger.trace("Current extension map: $extensions. Determining load order")
        for (entry in extensions) {
            addToLoadOrder(entry.key, entry.value)
        }
        logger.trace("Got load order $loadOrder")
    }

    fun extensionInstalled(id: String) = extensions.containsKey(id)

    fun extensionLoaded(id: String) = loadedExtensions.contains(id)

    fun extensionEnabled(id: String) = enabledExtensions.contains(id)

    fun getExtensionData(id: String) = extensions[id]

    fun removeExtension(id: String) {
        if (extensionInstalled(id)) {
            unenable(id)
            unload(id)
            extensions.remove(id)
        }
    }

    fun load() {
        for (id in loadOrder) {
            load(id)
        }
    }

    fun enable(application: Application) {
        for (id in loadOrder) {
            enable(id, application)
        }
    }

    fun unenable() {
        for (i in loadOrder.size..0) {
            unenable(loadOrder[i])
        }
    }

    fun unload() {
        for (i in loadOrder.size..0) {
            unload(loadOrder[i])
        }
    }

    private fun load(id: String) {
        if (!extensionInstalled(id)) return
        if (extensionLoaded(id)) return
        val extensionData = extensions[id]!!
        for (extension in extensionData.loadsAfter) {
            load(extension)
        }
        try {
            extensionData.extension.load(this)
            loadedExtensions.add(id)
            logger.trace("Loaded extension {}", id)
        } catch (ex: Exception) {
            logger.trace("Failed to load extension {}", id, ex)
        }
    }

    private fun enable(id: String, application: Application) {
        if (!extensionInstalled(id)) return
        if (extensionEnabled(id)) return
        val extensionData = extensions[id]!!
        for (extension in extensionData.loadsAfter) {
            enable(extension, application)
        }
        try {
            extensionData.extension.enable(this, application)
            enabledExtensions.add(id)
            logger.trace("Enabled extension {}", id)
        } catch (ex: Exception) {
            logger.trace("Failed to enable extension {}", id, ex)
            unload(id)
        }
    }

    private fun unenable(id: String) {
        if (!extensionInstalled(id)) return
        if (!extensionEnabled(id)) {
            return
        }
        val extensionData = extensions[id]!!
        if (extensionData.loadsBefore.size != 0) {
            for (i in (extensionData.loadsBefore.size - 1)..0) {
                unload(extensionData.loadsBefore[i])
            }
        }
        try {
            extensionData.extension.unenable(this)
        } catch (ex: Exception) {
            logger.trace("Failed to unenabled extension {}", id, ex)
        }
        enabledExtensions.remove(id)
        logger.trace("Unenabled extension {}", id)
    }

    private fun unload(id: String) {
        if (!extensionInstalled(id)) return
        if (!extensionLoaded(id)) {
            return
        }
        val extensionData = extensions[id]!!
        if (extensionData.loadsBefore.size != 0) {
            for (i in (extensionData.loadsBefore.size - 1)..0) {
                unload(extensionData.loadsBefore[i])
            }
        }
        try {
            extensionData.extension.unload(this)
        } catch (ex: Exception) {
            logger.trace("Failed to unload extension {}", id, ex)
        }
        loadedExtensions.remove(id)
        logger.trace("Unloaded extension {}", id)
    }

    private fun addToLoadOrder(id: String, extension: ExtensionData) {
        for (extensionName in extension.loadsAfter) {
            logger.trace("Adding $extensionName for $id")
            addToLoadOrder(extensionName, extensions[extensionName]!!)
        }
        if (loadOrder.contains(id)) {
            return
        }
        loadOrder.add(id)
    }
}