package tk.amplifiable.backend.extensions

import io.ktor.application.Application
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import tk.amplifiable.backend.services.Service
import tk.amplifiable.backend.services.ServiceRegistry

interface Extension {
    val logger: Logger
        get() = LoggerFactory.getLogger(this::class.simpleName)

    fun load(manager: ExtensionManager) {

    }

    fun enable(manager: ExtensionManager, application: Application) {

    }

    fun unload(manager: ExtensionManager) {

    }

    fun unenable(manager: ExtensionManager) {

    }

    fun registerService(service: Service) {
        ServiceRegistry.registerService(service)
    }
}