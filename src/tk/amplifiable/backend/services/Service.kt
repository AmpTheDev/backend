package tk.amplifiable.backend.services

import io.ktor.application.ApplicationCall
import io.ktor.http.HttpMethod
import tk.amplifiable.backend.endpoints.EndpointRegistry

interface Service {
    fun getName(): String

    fun setupUserAllowed(): Boolean = false

    fun registerEndpoints() {

    }

    fun registerEndpoint(
        url: String,
        method: HttpMethod,
        authenticated: Boolean = false,
        callback: suspend (call: ApplicationCall) -> Unit
    ) {
        EndpointRegistry.registerEndpoint(url, method, if (authenticated) this.getName() + "-token" else null, callback)
    }
}