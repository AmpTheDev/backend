package tk.amplifiable.backend.services;

import io.ktor.application.ApplicationCall;
import io.ktor.http.HttpMethod;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function2;
import org.jetbrains.annotations.NotNull;
import tk.amplifiable.backend.endpoints.EndpointRegistry;

import java.util.function.Consumer;

public abstract class JavaService implements Service {
    @Override
    public boolean setupUserAllowed() {
        return false;
    }

    @Override
    public void registerEndpoints() {

    }

    public void registerEndpoint(@NotNull String url, @NotNull HttpMethod method, @NotNull Consumer<ApplicationCall> callback) {
        registerEndpoint(url, method, callback, false);
    }

    public void registerEndpoint(@NotNull String url, @NotNull HttpMethod method, @NotNull Consumer<ApplicationCall> callback, boolean authenticated) {
        registerEndpoint(url, method, authenticated, (call, continuation) -> {
            callback.accept(call);
            return Unit.INSTANCE;
        });
    }

    @Override
    public void registerEndpoint(@NotNull String url, @NotNull HttpMethod method, boolean authenticated, @NotNull Function2<? super ApplicationCall, ? super Continuation<? super Unit>, ?> callback) {
        EndpointRegistry.Companion.registerEndpoint(url, method, authenticated ? getName() + "-token" : null, callback);
    }
}
