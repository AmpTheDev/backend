package tk.amplifiable.backend.services

class ServiceRegistry {
    companion object {
        val services = HashMap<String, Service>()

        fun registerService(service: Service) {
            if (service.getName().contains(':')) {
                throw IllegalArgumentException("Service name contains illegal character ':'")
            }
            services[service.getName()] = service
        }

        fun registerEndpoints() {
            for (service in services.values) {
                service.registerEndpoints()
            }
        }

        /*private fun registerServices() {
            registerService(MainService())
            registerService(AdminService())
        }

        init {
            registerServices()
        }*/
    }
}