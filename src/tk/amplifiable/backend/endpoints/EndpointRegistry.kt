package tk.amplifiable.backend.endpoints

import io.ktor.application.ApplicationCall
import io.ktor.http.HttpMethod

class EndpointRegistry {
    data class Endpoint(
        val url: String,
        val method: HttpMethod,
        val authentication: String?,
        val callback: suspend (call: ApplicationCall) -> Unit
    )

    companion object {
        val endpoints = ArrayList<Endpoint>()

        fun registerEndpoint(
            url: String,
            method: HttpMethod,
            authentication: String? = null,
            callback: suspend (call: ApplicationCall) -> Unit
        ) {
            endpoints.add(Endpoint(url, method, authentication, callback))
        }
    }
}