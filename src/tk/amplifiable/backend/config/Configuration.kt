package tk.amplifiable.backend.config

import com.google.gson.GsonBuilder
import tk.amplifiable.backend.utils.DatabaseType
import java.io.File
import java.io.FileReader
import java.io.FileWriter
import java.util.*

data class ConfigurationData(
    val testServer: Boolean = false,
    val hashSalt: String = UUID.randomUUID().toString(),
    val jwtRealm: String = UUID.randomUUID().toString(),
    val jwtSecret: String = UUID.randomUUID().toString() + UUID.randomUUID().toString() + UUID.randomUUID().toString() + UUID.randomUUID().toString(),
    val jwtIssuer: String = UUID.randomUUID().toString(),
    val setupUuid: UUID = UUID.randomUUID(),
    val emailAddress: String = "example@example.org",
    val emailUsername: String = "example@example.org",
    val emailPassword: String = "example1234!",
    val smtpHost: String = "smtp.example.org",
    val smtpPort: Int = 465,
    val sslOnConnect: Boolean = true,
    val rootEndpoint: String = "http://localhost:9090",
    val sqlHost: String = "localhost",
    val sqlDatabase: String = "ampapi",
    val sqlPort: Int = 5432,
    val sqlUser: String = "ampapi",
    val sqlPassword: String = "example1234!",
    val hashIterations: Int = 100000,
    val databaseType: DatabaseType = DatabaseType.POSTGRESQL
)

class Configuration(val file: File) {
    var data = ConfigurationData()

    init {
        load()
        save()
    }

    companion object {
        private val gson = GsonBuilder().setPrettyPrinting().serializeNulls().create()
    }

    fun load() {
        if (file.exists()) {
            val reader = FileReader(file)
            data = gson.fromJson(reader, ConfigurationData::class.java)
            reader.close()
        }
    }

    fun save() {
        val writer = FileWriter(file)
        gson.toJson(data, writer)
        writer.close()
    }
}