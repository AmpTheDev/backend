package tk.amplifiable.backend.authentication

import java.util.*

data class RevokeServicePayload(val uuid: UUID, val service: String)

data class GrantServicePayload(val uuid: UUID, val service: String)