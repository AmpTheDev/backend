package tk.amplifiable.backend.authentication

import io.ktor.auth.Principal
import org.apache.commons.codec.binary.Hex
import org.jetbrains.exposed.dao.Entity
import org.jetbrains.exposed.dao.EntityClass
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IdTable
import tk.amplifiable.backend.mainConfig
import java.util.*
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.PBEKeySpec

object Users : IdTable<UUID>("users") {
    val username = varchar("username", 64).uniqueIndex()
    val password = varchar("password", 101)
    val email = varchar("email", 64).uniqueIndex()
    val lastPasswordReset = long("lastPasswordReset")
    val services = text("services")
    val statusId = uuid("statusId")
    override val id = uuid("uuid").primaryKey().entityId()
}

class User(uuid: EntityID<UUID>) : Entity<UUID>(uuid), Principal {
    companion object : EntityClass<UUID, User>(Users) {
        const val ARRAY_SEPERATOR = ":"
    }

    var username by Users.username
    var password by Users.password
    var email by Users.email
    var lastPwReset by Users.lastPasswordReset
    var statusId by Users.statusId
    var services by Users.services.transform(
        { it.joinToString(ARRAY_SEPERATOR) },
        { it.split(ARRAY_SEPERATOR).toTypedArray() }
    )
}

fun hashPassword(password: String): String {
    val skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512")
    val spec = PBEKeySpec(
        password.toCharArray(),
        mainConfig.data.hashSalt.toByteArray(Charsets.UTF_8),
        mainConfig.data.hashIterations,
        128
    )
    val key = skf.generateSecret(spec)
    return Hex.encodeHexString(key.encoded)
}

/*class SQLUserSource(private val service: String = "default", private val isService: Boolean = false) : UserSource {
    override fun get(uuid: UUID) = transaction {
        User.findById(uuid)
    }

    override fun get(email: String) = transaction {
        User.find { Users.email eq email }.first()
    }

    override fun getByName(name: String) = User.find { Users.username eq name }.first()

    override fun makeServiceAccessible(uuid: UUID, service: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun isServiceAccessible(uuid: UUID, service: String): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun makeServiceInaccessible(uuid: UUID, service: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun changeEmail(uuid: UUID, newEmail: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun changeName(uuid: UUID, username: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun emailAvailable(email: String): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun nameAvailable(name: String): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun userExists(uuid: UUID): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun delete(uuid: UUID) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun setPassword(uuid: UUID, newUnhashed: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getHashedPassword(uuid: UUID): String? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun setLastReset(uuid: UUID, lastReset: Long) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getLastReset(uuid: UUID): Long? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}*/