package tk.amplifiable.backend.authentication

import java.util.*

data class NewPasswordPayload(val newPassword: String)

data class CreateUserPayload(
    val email: String,
    val username: String,
    val password: String,
    val accessibleServices: Array<String>
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as CreateUserPayload

        if (email != other.email) return false
        if (username != other.username) return false
        if (!accessibleServices.contentEquals(other.accessibleServices)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = email.hashCode()
        result = 31 * result + username.hashCode()
        result = 31 * result + accessibleServices.contentHashCode()
        return result
    }
}

data class ChangePasswordPayload(val uuid: UUID, val newPassword: String)