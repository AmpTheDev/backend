package tk.amplifiable.backend.authentication

data class AuthenticatePayload(val service: String = "default")