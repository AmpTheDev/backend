package tk.amplifiable.backend.authentication

import com.auth0.jwt.JWT
import com.auth0.jwt.JWTVerifier
import com.auth0.jwt.algorithms.Algorithm
import tk.amplifiable.backend.mainConfig
import java.util.*

object JwtConfig {
    const val pwValidityInMs = 36_000_00 // 1 hour
    private const val validityInMs = 36_000_00 * 6 // 6 hours
    private val algorithm = Algorithm.HMAC512(mainConfig.data.jwtSecret)

    val verifier: JWTVerifier = JWT
        .require(algorithm)
        .withIssuer(mainConfig.data.jwtIssuer)
        .build()

    fun makePasswordResetToken(user: UUID): String = JWT.create()
        .withSubject("Password Reset")
        .withIssuer(mainConfig.data.jwtIssuer)
        .withClaim("made_at", System.currentTimeMillis())
        .withClaim("uuid", user.toString())
        .withExpiresAt(Date(System.currentTimeMillis() + pwValidityInMs))
        .sign(algorithm)

    fun makeToken(service: String, user: User): String = JWT.create()
        .withSubject("Authentication")
        .withIssuer(mainConfig.data.jwtIssuer)
        .withClaim("service", service)
        .withClaim("id2", user.statusId.toString())
        .withClaim("id", user.id.toString())
        .withExpiresAt(getExpiration())
        .sign(algorithm)

    private fun getExpiration() = Date(System.currentTimeMillis() + validityInMs)
}