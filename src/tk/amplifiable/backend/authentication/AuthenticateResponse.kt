package tk.amplifiable.backend.authentication

import java.util.*

data class AuthenticateResponse(val uuid: UUID, val name: String, val token: String, val service: String)